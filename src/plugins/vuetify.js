import Vue from "vue";
import Vuetify from "vuetify/lib";
import foursquare from "@/components/icons/foursquare";
import settings from "@/components/icons/settings";
import hashtag from "@/components/icons/hashtag";
Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    values: {
      foursquare: { component: foursquare },
      settings: { component: settings },
      hashtag: { component: hashtag },
    },
  },
});
